package ru.edu.lesson6;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;
import java.util.stream.Stream;


/**
 * Travel Service.
 */
public class TravelService {

    // do not change type
    private final List<CityInfo> cities = new ArrayList<>();



    /**
     * Append city info.
     *
     * @param cityInfo - city info
     * @throws IllegalArgumentException if city already exists
     */

    public void add(CityInfo cityInfo) {
        if (cities.contains(cityInfo)){
            throw new IllegalArgumentException("Город с такими параметрами уже существует");
        }

        cities.add(cityInfo);
    }

    /**
     * remove city info.
     *
     * @param cityName - city name
     * @throws IllegalArgumentException if city doesn't exist
     */
    public void remove(String cityName) {
        CityInfo city = cities.stream()
                .filter(x -> x.getName().equalsIgnoreCase(cityName))
                .findAny().orElseThrow(() -> new IllegalArgumentException("Город с такими параметрами не найден"));

        cities.remove(city);
    }

    /**
     * Get cities names.
     */
    public List<String> citiesNames() {
        return cities.stream()
                .map(CityInfo::getName)
                .filter(Objects::nonNull)
                .collect(Collectors.toList());
    }

    /**
     * Получаем город по названию, проверяем наличие
     * для методов getDistance и getCitiesNear
     * @throws IllegalArgumentException если город не существует
     */
    private CityInfo getCityByName(String cityName) {
        CityInfo city = cities.stream()
                .filter(x -> x.getName().equalsIgnoreCase(cityName))
                .findAny().orElseThrow(() -> new IllegalArgumentException("Город с такими параметрами не найден"));
        return city;
    }

    /**
     * Get distance in kilometers between two cities.
     * https://www.kobzarev.com/programming/calculation-of-distances-between-cities-on-their-coordinates/
     *
     * @param srcCityName  - source city
     * @param destCityName - destination city
     * @throws IllegalArgumentException if source or destination city doesn't exist.
     */
    public int getDistance(String srcCityName, String destCityName) {

        final double EARTH_RADIUS = 6372.795;

        CityInfo srcCity = getCityByName(srcCityName);
        CityInfo destCity = getCityByName(destCityName);


        //  косинусы и синусы широт и разницы долгот
        double cosLatitudeSrcCity = Math.cos(srcCity.getPosition().getLatitude());
        double cosLatitudeDestCity = Math.cos(destCity.getPosition().getLatitude());
        double sinLatitudeSrcCity = Math.sin(srcCity.getPosition().getLatitude());
        double sinLatitudeDestCity = Math.sin(destCity.getPosition().getLatitude());

        double longitudeDiff = destCity.getPosition().getLongitude() - srcCity.getPosition().getLongitude();

        double cosLongitudeDiff = Math.cos(longitudeDiff);
        double sinLongitudeDiff = Math.sin(longitudeDiff);

        double y = Math.sqrt(Math.pow(cosLatitudeDestCity * sinLongitudeDiff, 2)
                + Math.pow(cosLatitudeSrcCity * sinLatitudeDestCity - sinLatitudeSrcCity
                    * cosLatitudeDestCity * cosLongitudeDiff, 2));

        double x = sinLatitudeSrcCity * sinLatitudeDestCity + cosLatitudeSrcCity * cosLatitudeDestCity * cosLongitudeDiff;

        double distanceToKM = Math.atan2(y,x) * EARTH_RADIUS;

        return (int)distanceToKM;
    }

    /**
     * Get all cities near current city in radius.
     *
     * @param cityName - city
     * @param radius   - radius in kilometers for search
     * @throws IllegalArgumentException if city with cityName city doesn't exist.
     */
    public List<String> getCitiesNear(String cityName, int radius) {
        getCityByName(cityName);

        List<String> citiesNear = cities.stream()
                .map(CityInfo::getName)
                .filter(name -> getDistance(cityName, name) <= radius && !Objects.equals(name, cityName))
                .collect(Collectors.toList());

        return citiesNear;
    }

    public int size(){
        return cities.size();
    }
}
