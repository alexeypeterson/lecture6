package ru.edu.lesson6;


public class AppTravelService {
    public static void main(String[] args) {
        TravelService travelService = new TravelService();

        GeoPosition geoPositiona = new GeoPosition("12(34'56'')", "76(54'32'')");
        CityInfo ekb = new CityInfo("Ekb", new GeoPosition("56(45'07'')", "60(57'00'')"));
        CityInfo ekb2 = new CityInfo("Pervouralsk", new GeoPosition("56(88'07'')", "59(87'00'')"));



        travelService.add(ekb);
        travelService.add(ekb2);
        System.out.println(travelService.citiesNames());
        System.out.println(travelService.getDistance("Ekb", "Pervouralsk"));
        System.out.println(travelService.getCitiesNear("Ekb",90));

        System.out.println(geoPositiona);

    }
}
