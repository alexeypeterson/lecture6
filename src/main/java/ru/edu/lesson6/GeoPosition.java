package ru.edu.lesson6;

import java.text.DecimalFormat;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Geo position.
 */
public class GeoPosition {

    /**
     * Широта в радианах.
     */
    private double latitude;

    /**
     * Долгота в радианах.
     */
    private double longitude;


    /**
     * Ctor.
     *
     * @param latitudeGradus  - latitude in gradus
     * @param longitudeGradus - longitude in gradus
     *                        Possible values: 55, 55(45'07''), 59(57'00'')
     */
    public GeoPosition(String latitudeGradus, String longitudeGradus) {
        // parse and set latitude and longitude in radian
        Stream<String> streamLatitude = Stream.of(latitudeGradus);

        List<Integer> listLatitude = streamLatitude
                .flatMap((p) -> Arrays.stream(p.split("''|'|\\(|\\)")))
                .map(Integer::parseInt)
                .collect(Collectors.toList());
        if(listLatitude.size() == 3){
            latitude = ((listLatitude.get(0) + (double) (listLatitude.get(1) + listLatitude.get(2)/60)/60 ) * Math.PI / 180) ;
        }else if (listLatitude.size() == 1){
            latitude = listLatitude.get(0) * Math.PI / 180;
        }else
            throw new IllegalArgumentException("Введены некорректные координаты");


        Stream<String> streamLongitude = Stream.of(longitudeGradus);

        List<Integer> listLongitude = streamLongitude
                .flatMap((p) -> Arrays.stream(p.split("''|'|\\(|\\)")))
                .map(Integer::parseInt)
                .collect(Collectors.toList());
        if(listLongitude.size() == 3){
            longitude = (listLongitude.get(0) + (double) (listLongitude.get(1) + listLongitude.get(2)/60)/60 ) * Math.PI / 180 ;
        }else if (listLongitude.size() == 1){
            longitude = listLongitude.get(0) * Math.PI / 180;
        }else
            throw new IllegalArgumentException("Введены некорректные координаты");
    }


    public double getLatitude() {
        return latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    @Override
    public String toString() {
        return "GeoPosition{" +
                "latitude=" + latitude +
                ", longitude=" + longitude +
                '}';
    }
}
