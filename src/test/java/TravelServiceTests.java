import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import ru.edu.lesson6.CityInfo;
import ru.edu.lesson6.GeoPosition;
import ru.edu.lesson6.TravelService;

import java.util.List;

public class TravelServiceTests {

    @Rule
    public ExpectedException exceptionRule = ExpectedException.none();

    private TravelService converter = new TravelService();

    /**
     * Проверка метода Add
     */
    @Test
    public void testAdd() {
        CityInfo ekb = new CityInfo("Ekb", new GeoPosition("56(45'07'')", "60(57'00'')"));

        Assert.assertEquals(converter.size(), 0);
        converter.add(ekb);
        Assert.assertEquals(converter.size(), 1);

        List<String> cityName = converter.citiesNames();
        Assert.assertEquals(cityName.get(0), "Ekb");
    }

    /**
     * Проверка метода Add на выброс исключения при добавлении дубликата города
     */
    @Test(expected = IllegalArgumentException.class)
    public void testAdd_Error() {
        CityInfo ekb = new CityInfo("Ekb", new GeoPosition("56(45'07'')", "60(57'00'')"));

        Assert.assertEquals(converter.size(), 0);
        converter.add(ekb);
        converter.add(ekb);

    }

    /**
     * Проверка метода Remove
     */
    @Test
    public void testRemove() {
        CityInfo ekb = new CityInfo("Ekb", new GeoPosition("56(45'07'')", "60(57'00'')"));
        CityInfo perv = new CityInfo("Pervouralsk", new GeoPosition("56(88'07'')", "59(87'00'')"));

        Assert.assertEquals(converter.size(), 0);
        converter.add(ekb);
        converter.add(perv);
        Assert.assertEquals(converter.size(), 2);
        converter.remove("Ekb");
        Assert.assertEquals(converter.size(), 1);

        List<String> cityName = converter.citiesNames();
        Assert.assertEquals(cityName.get(0), "Pervouralsk");
    }

    /**
     * Проверка метода Remove на выброс исключения при попытке удалить несуществующий город
     */
    @Test(expected = IllegalArgumentException.class)
    public void testRemove_Error() {
        CityInfo ekb = new CityInfo("Ekb", new GeoPosition("56(45'07'')", "60(57'00'')"));
        CityInfo perv = new CityInfo("Pervouralsk", new GeoPosition("56(88'07'')", "59(87'00'')"));

        Assert.assertEquals(converter.size(), 0);
        converter.add(ekb);
        converter.add(perv);
        Assert.assertEquals(converter.size(), 2);
        converter.remove("Moscow");
    }

    /**
     * Проверка метода citiesNames
     */
    @Test
    public void testCitiesNames(){
        CityInfo ekb = new CityInfo("Ekb", new GeoPosition("56(45'07'')", "60(57'00'')"));
        CityInfo perv = new CityInfo("Pervouralsk", new GeoPosition("56(88'07'')", "59(87'00'')"));

        Assert.assertEquals(converter.size(), 0);
        converter.add(ekb);
        converter.add(perv);
        Assert.assertEquals(converter.size(), 2);
        List<String> cityName = converter.citiesNames();
        String name1 = cityName.get(0);
        String name2 = cityName.get(1);

        Assert.assertEquals(name1, "Ekb");
        Assert.assertEquals(name2, "Pervouralsk");

    }

    /**
     * Проверка метода getDistance
     */
    @Test
    public void testGetDistance(){
        CityInfo ekb = new CityInfo("Ekb", new GeoPosition("56(45'07'')", "60(57'00'')"));
        CityInfo perv = new CityInfo("Pervouralsk", new GeoPosition("56(88'07'')", "59(87'00'')"));

        converter.add(ekb);
        converter.add(perv);

        int distance = converter.getDistance("Ekb","Pervouralsk");

        Assert.assertEquals(distance, 85);

    }


    @Test
    public void testGetCitiesNear(){
        CityInfo ekb = new CityInfo("Ekb", new GeoPosition("56(45'07'')", "60(57'00'')"));
        CityInfo perv = new CityInfo("Pervouralsk", new GeoPosition("56(88'07'')", "59(87'00'')"));
        CityInfo nev = new CityInfo("Nevyansk", new GeoPosition("57(11'03'')", "60(99'00'')"));
        CityInfo msk = new CityInfo("Moscow", new GeoPosition("55(88'07'')", "37(87'00'')"));

        converter.add(ekb);
        converter.add(perv);
        converter.add(nev);
        converter.add(msk);

        List<String> cityNearList = converter.getCitiesNear("Ekb", 100);
        Assert.assertEquals(cityNearList.size(), 2);

    }
}
