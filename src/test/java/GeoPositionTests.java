import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import ru.edu.lesson6.GeoPosition;

public class GeoPositionTests {

    @Rule
    public ExpectedException exceptionRule = ExpectedException.none();

    /**
     * Проверка testGeoPosition
     */
    @Test
    public void testGeoPosition() {
        GeoPosition geoPositionTest = new GeoPosition("12(34'56'')", "76(54'32'')");
        double exeptedLat = geoPositionTest.getLatitude();
        double exeptedLong = geoPositionTest.getLongitude();
        double actualLat = 0.21932970933395404;
        double actualLong = 1.3421581947836394;

        Assert.assertEquals(exeptedLat, actualLat, 0.0);
        Assert.assertEquals(exeptedLong, actualLong, 0.0);

    }

    /**
     * Проверка testGeoPosition на выброс исключения
     */
    @Test(expected = IllegalArgumentException.class)
    public void testGeoPosition_Error() {
        GeoPosition geoPositionTest = new GeoPosition("12(34')", "76(54'32'')");
    }
}
